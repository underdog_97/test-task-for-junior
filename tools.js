const readline = require('readline-sync');

module.exports = {
    clientInput: () => {
        let matrixHeight, matrixWidth, wallshapeMatrix = [], wallshapeBricks = [], bricks = [];
        let emptyBrick = '[   ]';
        let brick = '[ + ]';
        let badBrick = '[ - ]'
        let heightWidth;

        let promise = new Promise(function(resolve, reject) {
            new Promise((resolve, reject) => {
                let matrixQuestion = () => {
                    heightWidth = readline.question('Please enter width and height of wall\'s shape matrix. Two integers separated by space on their own line!\nExample: 6 3\n');
                    heightWidth.length === 3 && heightWidth[1] === ' ' && +heightWidth[0] > 0 && +heightWidth[2] > 0 ? resolve() : matrixQuestion();
                }
                matrixQuestion();
            })
            .then(() => {
                matrixHeight = heightWidth.split(' ')[1];
                matrixWidth = heightWidth.split(' ')[0];
                for (let i = 0; i < matrixHeight; i++) {
                    console.log(emptyBrick.repeat(matrixWidth));
                }
            })
            resolve();
        });

        promise.then(
            res => {
                console.log('Please enter string, formed just of \'1\' and \'0\'. Length of this string must equal to width of matrix. Example: 101101\n');
                for (let j = 0; j < matrixHeight; j++) {
                    let matrixForm = () => {
                        let readLineResTemp = readline.question(`${j+1} line:`);
                        matrixWidth == readLineResTemp.length && +readLineResTemp[0] === 1 || 0 ? wallshapeMatrix.push(readLineResTemp) : matrixForm();
                    }
                    matrixForm();
                }
                wallshapeMatrix.forEach(element => {
                    element.split('').forEach(sub => {
                        sub === '1' ? wallshapeBricks.push(brick) : wallshapeBricks.push(badBrick);
                    });
                    wallshapeBricks.push('\n');
                })
                console.log(`Your matrix got this form.\n${wallshapeBricks.join('')}`);
            },
        );
        promise.then(
            res => {
                let bricksCount;
                let enterCountBricks = () => {
                    console.log('Please enter count of bricks\' sorts. Example: 4\n');
                    bricksCount = readline.question(`Count of bricks:`);
                    +bricksCount > 0 && Number.isInteger(+bricksCount) ? console.log('') : enterCountBricks(); 
                }
                enterCountBricks();
                let regex1 = /^\d+ \d+ \d+$/;
                
                console.log('List of bricks. Enter three positive integers separated by space - width of brick, height of brick, such bricks in the set. Example: 1 1 4\n2 1 6\n');
                    for (let i = 0; i < bricksCount; i++) {
                        let enterList = () => {
                            let readLineResTemp = readline.question(`${i+1} brick:`);
                            if (regex1.test(readLineResTemp) && +readLineResTemp.split(' ')[0] < 9
                                 && +readLineResTemp.split(' ')[0] > 0 && +readLineResTemp.split(' ')[1] > 0
                                 && +readLineResTemp.split(' ')[1] < 9 && +readLineResTemp.split(' ')[2] > 0) {
                                for (let j = 0; j < readLineResTemp.split(' ')[2]; j++) {
                                    bricks.push(
                                        {
                                            param1: readLineResTemp.split(' ')[0],
                                            param2: readLineResTemp.split(' ')[1],
                                        }
                                    );
                                }
                            }
                            else enterList();
                        }    
                        enterList();
                    }
                })
        return [wallshapeMatrix, bricks];
    },
    checkHorizontal: (x, y, brickParam1, brickParam2, array) => {
        let tempArray = [];
        for (let i = x; i < Math.min(brickParam1, brickParam2) + x; i++) {
            for (let j = y; j < Math.max(brickParam1, brickParam2) + y; j++) {
                try {
                    tempArray.push(array[i][j]);
                }
                catch {}
            }
        }
        return tempArray.every(isEqualToOne) && tempArray.length === brickParam1 * brickParam2 ? true : false;
    },
    checkVertical: (x, y, brickParam1, brickParam2, array) => {
        let tempArray = [];
        for (let i = x; i < Math.max(brickParam1, brickParam2) + x; i++) {
            for (let j = y; j < Math.min(brickParam1, brickParam2) + y; j++) {
                try {
                    tempArray.push(array[i][j]);
                }
                catch {}
            }
        }
        return tempArray.every(isEqualToOne) && tempArray.length === brickParam1 * brickParam2 ? true : false;
    },
    fillSchemeHorizontal: (x, y, brickParam1, brickParam2, array) => {
        let tempArray = [];
        for(let z = 0; z < array.length; z++) {
            tempArray.push(array[z].split(''));
        }
        array = [];
        for (let i = x; i < Math.min(brickParam1, brickParam2) + x; i++) {
            for (let j = y; j < Math.max(brickParam1, brickParam2) + y; j++) {
                tempArray[i][j] = '0';
            }
        }
        for(let z = 0; z < tempArray.length; z++) {
            array.push(tempArray[z].join(''));
        }
        return array;
    },
    fillSchemeVertical: (x, y, brickParam1, brickParam2, array) => {
        let tempArray = [];
        for(let z = 0; z < array.length; z++) {
            tempArray.push(array[z].split(''));
        }
        array = [];
        for (let i = x; i < Math.max(brickParam1, brickParam2) + x; i++) {
            for (let j = y; j < Math.min(brickParam1, brickParam2) + y; j++) {
                tempArray[i][j] = '0';
            }
        }
        for(let z = 0; z < tempArray.length; z++) {
            array.push(tempArray[z].join(''));
        }
        return array;
    },
    callbackSort: (a, b) => {
        if (a.param1 + a.param2 > b.param1 + b.param2) {
            return -1;
        }
        else if (a.param1 + a.param2 < b.param1 + b.param2) {
            return 1;
        }
        else {
            return 0;
        }
    }
}

function isEqualToOne(element) {
    return element === '1';
}