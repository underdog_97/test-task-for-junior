const tools = require('./tools'); // adding functions from another file.
let wallshapeMatrix = [], bricks = [], resultArray = [], inputRes;


//  using promise for sync all next actions.
let promise = new Promise((resolve, reject) => {
    inputRes = tools.clientInput();
    resolve();
});
  
promise.then(() => {
        wallshapeMatrix = inputRes[0];
        bricks = inputRes[1];
        bricks.sort(tools.callbackSort); // sorting inputed bricks by params from biggest to lowest.

        for (let x = 0; x < bricks.length; x++) {
            loop1: // label "loop1" added for breaking loop from nested loop.
            for (let i = 0; i < wallshapeMatrix.length; i++) {
                for (let j = 0; j < wallshapeMatrix[i].length; j++) {
                    if (tools.checkHorizontal(i, j, +bricks[x].param1, +bricks[x].param2, wallshapeMatrix)) {
                        wallshapeMatrix = tools.fillSchemeHorizontal(i, j, +bricks[x].param1, +bricks[x].param2, wallshapeMatrix);
                        resultArray.push('+');
                        break loop1; // break outside loop from nested loop.
                    }
                    if (tools.checkVertical(i, j, +bricks[x].param1, +bricks[x].param2, wallshapeMatrix)) {
                        wallshapeMatrix = tools.fillSchemeVertical(i, j, +bricks[x].param1, +bricks[x].param2, wallshapeMatrix);
                        resultArray.push('+');
                        break loop1; // break outside loop from nested loop.
                    }
                }
            }
        }
    });

promise.then(() => {
    bricks.length === resultArray.length ? console.log('yes') : console.log('no');
})

